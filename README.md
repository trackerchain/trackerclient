- [简介](#简介)
- [功能界面](#功能界面)
- [运行环境](#运行环境)
- [使用说明](#使用说明)
  - [一、下载tracker](#下载tracker)
  - [二、配置环境变量](#配置环境变量)
  - [三、启动tracker](#启动tracker)
  - [四、访问tracker](#访问tracker)
- [策略开发](#策略开发)
  - [一、目录结构](#目录结构)
  - [二、新建策略](#新建策略)
  - [三、编写策略](#编写策略)
    - [代码结构](#代码结构)
    - [配置文件](#配置文件)
    - [使用GoLand开发](#使用GoLand开发)
  - [四、编译发布策略](#编译发布策略)
  - [五、策略方法](#策略方法)
    - [获取合约信息](#获取合约信息)
    - [获取Tick行情](#获取Tick行情)
    - [获取K线行情](#获取K线行情)
    - [获取合约乘数](#获取合约乘数)
    - [获取最小变动价](#获取最小变动价)
    - [获取合约所在交易所](#获取合约所在交易所)
    - [获取自定义参数](#获取自定义参数)

## 简介

<div align="center">  
 <img src="https://image.hrrdqh.com/tracker.png" alt="trackertrader" align=left />
</div>
<br/>
<br/>
<br/>
<br/>

> 我们在研究量化交易的过程中，发现市面上现有的量化交易系统（vnpy等）并不满足自身需求。我们的需求主要有以下几个方面：
1. 支持多策略同时运行，支持多合约行情和交易，支持历史回测
2. 运行性能极高并且开发速度快，要有区别于python技术体系
3. 聚焦期货交易，而不是一个大而全的系统
4. 使用配置足够简单，傻瓜式上手
5. 策略开发高度自定义，开发灵活
6. 界面显示聚焦核心内容且操作简单
7. 系统可以独立部署，有极高的安全性
> 基于以上需求，我们使用go语言开发出了量化交易客户端（tracker）、行情服务端（tracker-quoteserver）和交易服务端（tracker-tradeserver），三端均可以独立部署。

## 功能界面

<div align="center">
  <img src="https://image.hrrdqh.com/tracker1.png" align=center />
  策略监控主界面
  <br/>
  <br/>
  <img src="https://image.hrrdqh.com/tracker2.png" align=center />
  策略详情
  <br/>
  <br/>
  <img src="https://image.hrrdqh.com/tracker3.png" align=center />
  策略回测
  <br/>
  <br/>
  <img src="https://image.hrrdqh.com/tracker4.png" align=center />
  策略回测盈亏曲线
  <br/>
  <br/>
  <img src="https://image.hrrdqh.com/tracker5.png" align=center />
  策略回测胜率曲线
  <br/>
  <br/>
</div>

## 运行环境

| Window | Windows 7、Windows 10+             |
|--------|-----------------------------------|
| Linux  | Ubuntu 18.04+、Debian 8+、CentOS 8+ |
| Go     | Go 1.18.0+                        |
go下载地址：https://golang.google.cn/dl/

## 使用说明

### 一、下载tracker

1. 下载tracker-v0.1.0-windows.zip<div><img src="https://image.hrrdqh.com/tracker_release1.png"/><img src="https://image.hrrdqh.com/tracker_release2.png"/></div>
2. 解压缩tracker-v0.1.0-windows.zip到d:\tracker，得到文件目录<img src="https://image.hrrdqh.com/tracker_release3.png" width="300" />
3. 目录结构：

| 目录/文件 | 说明                              |
| ----------- | ----------------------------------- |
| bin       | tracker可执行文件以及依赖的动态库 |
| build     | 策略开发和编译目录                |
| conf      | 配置文件目录                      |
| plugin    | 策略插件运行目录                  |
| sqlite3   | 数据库文件目录                    |
| webui     | web界面目录                       |
| start.bat | 运行tracker批处理文件             |

### 二、配置环境变量

在我的电脑->高级系统设置，新建环境变量
TRACKER_HOME=d:\tracker

<div><img src="https://image.hrrdqh.com/tracker_release4.png" width="550" /></div>
<div><img src="https://image.hrrdqh.com/tracker_release5.png" width="550" /></div>

### 三、启动tracker

双击运行tracker目录下的start.bat
<img src="https://image.hrrdqh.com/tracker_release6.png" />

### 四、访问tracker

在浏览器地址栏输入"http://localhost:8080" 打开登录界面输入以下信息即可登录<br/>

> 用户名：test<br/>
> 密码：test<br/>
> 口令：test<br/>

<img src="https://image.hrrdqh.com/tracker_release7.png" />
<img src="https://image.hrrdqh.com/tracker_release8.png" />

### 五、选择策略

点击“策略库”，选择trenddemo策略，点击鼠标右键选择“监控”即可
<img src="https://image.hrrdqh.com/tracker_release9.png" />

### 六、启动策略

点击“启动”即可启动策略
<img src="https://image.hrrdqh.com/tracker_release10.png" />

## 策略开发

### 一、目录结构

build是开发策略的目录

<img src="https://image.hrrdqh.com/tracker_release11.png" width="300" />

| 目录/文件    | 说明             |
| -------------- | ------------------ |
| templete     | 策略模板         |
| trenddemo    | 演示策略         |
| build.bat    | 编译演示策略脚本 |
| templete.bat | 编译脚本模板     |

### 二、新建策略

复制“build\templete”文件夹新建一个，将名字改为新策略的名字，例如trendtest，将trendtest文件夹下的templete.go和templete.toml改为trendtest.go和trendtest.toml，打开trendtest.go代码文件，将Templete结构改为Trendtest（首字母大写），将templete结构改为trendtest，这样新策略开发目录创建完成，可以在此基础上开发策略。
<img src="https://image.hrrdqh.com/tracker_release12.png" width="350" />
<img src="https://image.hrrdqh.com/tracker_release13.png" width="350" />
<img src="https://image.hrrdqh.com/tracker_release15.png" width="600" />

### 三、编写策略

- #### 代码结构

<img src="https://image.hrrdqh.com/tracker_release16.png" />
<img src="https://image.hrrdqh.com/tracker_release17.png" />

代码主要在OnInit（初始化策略）、OnTick（quote行情）、OnKline（K线行情）中编写。

- #### 配置文件

"trendtest.toml"是trendtest策略的配置文件。

<img src="https://image.hrrdqh.com/tracker_release23.png" width="200" />


| 参数   | 说明                                                                                                                          |
| -------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Desc   | 策略描述                                                                                                                      |
| Period | 策略挂载周期：0是quote行情报价；1是1分钟K线；5是5分钟K线；10是10分钟K线；15是15分钟K线；30是30分钟K线；60是60分钟K线；d是日线 |
| Length | 行情缓存长度，例如配置成200，Quote缓存器和K线缓存器里就有200根行情数据，在策略里可以根据长度取行情                            |
| Main   | 主合约，这是一个数组，第一位是行情合约代码，第二位是交易合约代码                                                              |
| Sub    | 次合约，这是一个数组，第一位是行情合约代码，第二位是交易合约代码                                                              |
| Extend | 用户自定义指标扩展参数，在策略里可以生成自定义指标，界面中可以显示出来                                                        |

> #### 使用GoLand开发

<img src="https://image.hrrdqh.com/tracker_release18.png" />

### 四、编译发布策略

复制"build\templete.bat"新建一个新的，改名为"trendtest.bat"，打开文件将templete替换为trendtest，双击运行"trendtest.bat"即可自动编译和发布，运行完毕后在"plugin"目录下会生成"trendtest"策略目录，编译发布完成。点击界面里的策略库按钮即可监控策略。

<img src="https://image.hrrdqh.com/tracker_release21.png" width="450" />

创建"trendtest.bat"文件

<img src="https://image.hrrdqh.com/tracker_release19.png" width="450" />

将templete替换为trendtest

<img src="https://image.hrrdqh.com/tracker_release20.png" width="450" />

双击运行"trendtest.bat"在"plugin"目录下生成trendtest目录

<img src="https://image.hrrdqh.com/tracker_release22.png" />

点击"策略库"按钮添加trendtest策略监控

### 五、策略方法
- #### 获取合约信息
<img src="https://image.hrrdqh.com/tracker_release24.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release25.png" width="500" />

- #### 获取Tick行情
<img src="https://image.hrrdqh.com/tracker_release26.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release27.png" width="500" />

- #### 获取K线行情
<img src="https://image.hrrdqh.com/tracker_release28.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release29.png" width="500" />

- #### 获取合约乘数
<img src="https://image.hrrdqh.com/tracker_release30.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release31.png" width="500" />

- #### 获取最小变动价
<img src="https://image.hrrdqh.com/tracker_release32.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release33.png" width="500" />

- #### 获取合约所在交易所
<img src="https://image.hrrdqh.com/tracker_release34.png" width="500" />

<img src="https://image.hrrdqh.com/tracker_release35.png" width="500" />

- #### 获取自定义参数
<img src="https://image.hrrdqh.com/tracker_release36.png" width="500" />

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
