package main

import (
	"fmt"
	"gitee.com/trackertrader/trackersdk/common"
	"gitee.com/trackertrader/trackersdk/strategy"
	"gitee.com/trackertrader/trackersdk/utils"
	"github.com/BurntSushi/toml"
	"github.com/fsnotify/fsnotify"
	"github.com/hashicorp/go-plugin"
	"math"
	"os"
	"path"
	"strings"
)

var config RiskConfig

type RiskFunction struct {
	strategy.Riskbase
	ConfigFileName string
}

// RiskConfig 自定义配置文件
type RiskConfig struct {
	InitCash float64 `toml:"initcash"`
	N        float64 `toml:"N"`
	C        float64 `toml:"C"`
	F        float64 `toml:"F"`
}

// OnInit 初始化
func (r *RiskFunction) OnInit() {
	Am := config.InitCash * config.N
	An := Am
	valueM, err0 := r.GetParamFloat(r.GetName(), "Amax")
	valueN, err1 := r.GetParamFloat(r.GetName(), "Anow")
	valueR, err2 := r.GetParamFloat(r.GetName(), "Ar")
	if nil != err0 || nil != err1 || nil != err2 {
		r.Log(common.ALARMERROR, err0.Error()+err1.Error()+err2.Error())
		return
	}
	if valueM == 0 || valueN == 0 {
		// 第一次计算: 进行Am An初始化
		_ = r.SetParamFloat("Amax", Am)
		_ = r.SetParamFloat("Anow", An)
	} else {
		// 连续计算: 读取上一个 Am,An
		Am = valueM
		An = valueN
	}
	if valueR == 0 {
		Ar := (An * An / Am) * config.F
		_ = r.SetParamFloat("Ar", Ar)
	}
}

// RiskControl 编写风控函数
func (r *RiskFunction) RiskControl() string {
	valueM, err0 := r.GetParamFloat(r.GetName(), "Amax")
	valueN, err1 := r.GetParamFloat(r.GetName(), "Anow")
	if nil != err0 || nil != err1 {
		r.Log(common.ALARMERROR, err0.Error()+err1.Error())
		return err0.Error() + err1.Error()
	}
	Am := config.InitCash * config.N
	An := Am
	if valueM == 0 || valueN == 0 {
		// 第一次计算: 进行Am An初始化
		_ = r.SetParamFloat("Amax", Am)
		_ = r.SetParamFloat("Anow", An)
	} else {
		// 连续计算: 读取上一个 Am,An
		Am = valueM
		An = valueN
	}

	B := r.LastCloseProfit // 获得上一次平仓收益
	tmp := An + B          // An + 平仓收益
	// 计算新的An
	Anew := tmp - config.C*math.Max(tmp-Am, 0.0)
	r.Log(common.ALARMINFO, fmt.Sprintf("tmp: %f = An: %f + LastCloseProfit: %f) ", tmp, An, B))
	r.Log(common.ALARMINFO, fmt.Sprintf("Anew: %f = tmp: %f + %f * max(tmp: %f - Am: %f, 0) ", Anew, tmp, config.C, tmp, Am))

	///////////////
	// 特殊情况, 调整C值
	OpenRV, err := r.GetParamFloat(r.LastStaName, r.LastSymbolT+"OpenRV")
	if nil != err {
		return err.Error()
	}
	// 能读出Atr就调整, 不能读出来就不调整; 能读出来就进入下面的C值调整模式
	if OpenRV > 0.0001 {
		r.Log(common.ALARMINFO, fmt.Sprintf("LastCloseGain: %f; OpenRV: %f) ", r.LastCloseGain, OpenRV))
		amp := r.LastCloseGain / OpenRV // 算出收益是开仓RV的多少倍
		if amp > 10.0 {
			partC := 10.0 / amp  // 保留10倍以内的部分
			partX := 1.0 - partC // 对10倍以上的部分取 0.1后 重新计算B值
			B = (r.LastCloseProfit * partC) + (0.1 * partX * r.LastCloseProfit)
			tmp = An + B // 重新计算 An + B
			Anew = tmp - config.C*math.Max(tmp-Am, 0.0)
		}
	}
	///////////////

	// 新的An破新高的情况下, 更新 Am
	if Anew > Am {
		Am = Anew
		_ = r.SetParamFloat("Amax", Am)
	}
	// 记录新的 An
	An = Anew
	_ = r.SetParamFloat("Anow", An)
	// 计算单笔风险金Ar
	Ar := 0.0
	if Am == 0 {
		return "!!!: Amax 为 0"
	} else {
		Ar = (An * An / Am) * config.F
		_ = r.SetParamFloat("Ar", Ar)
	}
	return ""
}

func main() {
	// 实例化策略
	riskfunction := &RiskFunction{}
	riskfunction.RegOnInit(riskfunction.OnInit)

	// 加载配置文件
	fullName, _ := os.Executable()
	fullName = strings.ReplaceAll(fullName, "\\", "/")
	dirName := path.Dir(fullName)
	pluginName := path.Base(dirName)
	riskfunction.ConfigFileName = dirName + "/" + pluginName + ".toml"
	if !utils.IsFileExists(riskfunction.ConfigFileName) {
		return
	}
	if _, err := toml.DecodeFile(riskfunction.ConfigFileName, &config); err != nil {
		return
	}
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return
	}
	defer watcher.Close()
	go func() {
		for {
			select {
			case ev, ok := <-watcher.Events:
				if !ok {
					return
				}
				if ev.Op&fsnotify.Write == fsnotify.Write {
					_, _ = toml.DecodeFile(riskfunction.ConfigFileName, &config)
				}
			}
		}
	}()
	err = watcher.Add(riskfunction.ConfigFileName)
	if err != nil {
		return
	}

	// pluginMap is the map of plugins we can dispense.
	var pluginMap = map[string]plugin.Plugin{
		pluginName: &strategy.RiskPlugin{Impl: riskfunction},
	}
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: strategy.HandshakeConfig,
		Plugins:         pluginMap,
	})
}
