package main

import (
	"fmt"
	"gitee.com/trackertrader/trackersdk/common"
	"gitee.com/trackertrader/trackersdk/strategy"
	"gitee.com/trackertrader/trackersdk/utils"
	log "github.com/dounetio/slog"
	"github.com/hashicorp/go-plugin"
	"math"
	"os"
	"path"
	"strconv"
	"strings"
)

type TurtleTrading struct {
	strategy.Base

	// 合约信息应读取的全局变量
	symbolt   string
	mutiplier float64
	minmove   float64
	mainkey   common.KlineKey
	subkey    common.KlineKey
	// 配置文件内应添加的参数变量
	LenS       int64
	LenL       int64
	BreakLen   int64
	CoverLen   int64
	ClrWt      float64
	RiskBudget float64
	// 策略管理器内应添加的策略公共变量
	BarCntr int64

	// 策略自定义的公共变量A ---- (高通用性)
	SimPositionRate float64
	InPrice         float64
	ClrPrice        float64
	InPosRV         float64
	InPos           float64
	max_Hs          float64
	min_Ls          float64
	PosAdaptor      float64
	RiskAct         float64
	ClrThr          float64
	TradePrc        float64
	max_coverHs     float64
	min_coverLs     float64
	// 策略自定义的公共变量B ---- (高私有性)
	TR        []float64
	ATR       []float64
	RV        []float64
	ShortLine []float64
	LongLine  []float64
}

/**
 * @Description: 初始化策略
 * @receiver s
 * @param cctx
 * @return error
 */
func (s *TurtleTrading) OnInit() {
	log.Info("TurtleTrading Init")

	// ------------- 初始化策略变量 ---------------- //
	// 合约信息应读取的全局变量
	s.symbolt = s.Contract.MainKline.SymbolT
	s.mutiplier = float64(s.Multiplier[s.symbolt])
	s.minmove = s.MinMove[s.symbolt]
	s.mainkey = s.Contract.MainKline
	s.subkey = s.Contract.SubKline
	// 配置文件内应添加的参数变量
	s.LenS = s.Extend["lens"].(int64)
	s.LenL = s.Extend["lenl"].(int64)
	s.BreakLen = s.Extend["break_len"].(int64)
	s.CoverLen = s.Extend["cover_len"].(int64)
	s.ClrWt = s.Extend["ClrWt"].(float64)
	s.RiskBudget = s.Extend["risk"].(float64)
	// 策略管理器内应添加的策略公共变量
	s.BarCntr = 0
	// 策略自定义的公共变量A ---- (高通用性)
	s.SimPositionRate = 0.0
	s.InPrice = 0.0
	s.ClrPrice = 0.0
	s.max_Hs = 0.0
	s.min_Ls = 0.0
	s.max_coverHs = 0.0
	s.min_coverLs = 0.0
	s.InPos = 0.0
	s.InPosRV = 0.0
	s.PosAdaptor = 1.0
	s.RiskAct = s.RiskBudget
	s.ClrThr = 0.0
	s.TradePrc = 0.0
	// 策略自定义的公共变量B ---- (高私有性)
	s.TR = make([]float64, 100)
	s.ATR = make([]float64, 100)
	s.RV = make([]float64, 100)
	s.ShortLine = make([]float64, 100)
	s.LongLine = make([]float64, 100)
}

/**
 * @Description:tick行情
 * @receiver s
 * @param symbol 合约代码
 * @param quote tick行情
 */
func (s *TurtleTrading) OnTick(symbol string, quote common.Quote) {

}

/**
 * @Description: K线行情
 * @receiver s
 * @param symbol 合约代码
 * @param period k线周期
 * @param kline k线数据
 * @param flag 是否是新增k线
 */
func (s *TurtleTrading) OnKline(klinekey common.KlineKey, kline common.Kline, offsetflag bool) {
	if s.BreakLen > 199 {
		s.BreakLen = 199
	}
	// -------------------- K线行情触发 ----------------------- //
	if klinekey.Symbol == s.Contract.MainKline.Symbol {
		if offsetflag {
			log.Infof("Symbol: %s, ||||| datetime: %s, Close: %f", klinekey.Symbol, s.K.Time[klinekey][0].Format("2006-01-02 15:04:05"), s.K.Close[klinekey][0])

			// Offset --------------------- 当K线位移更新时
			C200s := s.K.Close[klinekey][1:200]
			Hs200s := s.K.High[klinekey][1:200]
			Ls200s := s.K.Low[klinekey][1:200]

			// 突破价格
			s.max_Hs = math.Round(utils.MaxArrayFloat(Hs200s[0:s.BreakLen])*100.0) * 0.01
			s.min_Ls = math.Round(utils.MinArrayFloat(Ls200s[0:s.BreakLen])*100.0) * 0.01

			// 平仓价格
			s.max_coverHs = math.Round(utils.MaxArrayFloat(Hs200s[0:s.CoverLen])*100.0) * 0.01
			s.min_coverLs = math.Round(utils.MinArrayFloat(Ls200s[0:s.CoverLen])*100.0) * 0.01

			TR := utils.TrueRange(C200s[0:s.BreakLen+1], Hs200s[0:s.BreakLen+1], Ls200s[0:s.BreakLen+1])
			sd := std(TR[0:s.BreakLen])
			ATR := utils.MovingAverage(TR, int(s.BreakLen))
			s.ATR = utils.AppendFloat(s.ATR, ATR[0], true)
			valid := math.Max(2*ATR[0], ATR[0]+2.58*sd)
			if valid > 5*s.minmove {
				s.RV = utils.AppendFloat(s.RV, valid, true)
			} else {
				s.RV = utils.AppendFloat(s.RV, 5*s.minmove, true)
			}

			ShortMA := utils.MovingAverage(C200s, int(s.LenS))
			LongMA := utils.MovingAverage(C200s, int(s.LenL))
			s.ShortLine = utils.AppendFloat(s.ShortLine, ShortMA[0], true)
			s.LongLine = utils.AppendFloat(s.LongLine, LongMA[0], true)

			// (1) 开仓条件 :  act_flg -----------------------------------------------------------
			act_flg := 0
			if ShortMA[0] > LongMA[0] && s.K.High[klinekey][0] > s.max_Hs {
				act_flg = 1
			} else if ShortMA[0] < LongMA[0] && s.K.Low[klinekey][0] < s.min_Ls {
				act_flg = -1
			}

			// (2) 平仓条件 :  clr_flg --------------------------------------------------------------
			clr_flg := false
			// (3) 平仓/减仓 --------------------------------------------------------------
			if s.GetMainVirtualPosition() > 0 {
				if s.K.Open[klinekey][0] < s.ClrThr {
					s.ClrPrice = s.K.Open[klinekey][0]
					clr_flg = true
				} else if s.K.Low[klinekey][0] < s.ClrThr {
					s.ClrPrice = s.ClrThr - s.minmove
					clr_flg = true
				}
				if s.K.Open[klinekey][0] < s.min_coverLs {
					s.ClrPrice = s.K.Open[klinekey][0]
					clr_flg = true
				} else if s.K.Low[klinekey][0] < s.min_coverLs {
					s.ClrPrice = s.min_coverLs - s.minmove
					clr_flg = true
				}
			} else if s.GetMainVirtualPosition() < 0 {
				if s.K.Open[klinekey][0] > s.ClrThr {
					s.ClrPrice = s.K.Open[klinekey][0]
					clr_flg = true
				} else if s.K.High[klinekey][0] > s.ClrThr {
					s.ClrPrice = s.ClrThr + s.minmove
					clr_flg = true
				}
				if s.K.Open[klinekey][0] > s.max_coverHs {
					s.ClrPrice = s.K.Open[klinekey][0]
					clr_flg = true
				} else if s.K.High[klinekey][0] > s.max_coverHs {
					s.ClrPrice = s.max_coverHs + s.minmove
					clr_flg = true
				}
			}

			// (4) 开仓 ----------------------------------------------------------------------------------
			if act_flg == 1 {
				if s.GetMainVirtualPosition() == 0 {
					s.SimPositionRate = 1
					//s.RiskAct = s.RiskBudget
				}
			} else if act_flg == -1 {
				if s.GetMainVirtualPosition() == 0 {
					s.SimPositionRate = -1.0
					//s.RiskAct = s.RiskBudget
				}
			}
			if act_flg != 0 {
				// 读取风控模块 RiskControl 的 风险参数 (Running 状态下)
				if common.RUNNING == s.GetState() {
					Ar, err := s.GetParamFloat("RiskFunction", "Ar")
					if nil != err {
						s.Log(common.ALARMERROR, err.Error())
						s.RiskAct = 0
					} else {
						s.RiskAct = Ar
						s.Log(common.ALARMINFO, fmt.Sprintf("RiskAct: %f = Ar: %f) ", s.RiskAct, Ar))
					}
				}
			}
			// 读取风控模块 RiskControl 的 风险参数	(Loading 状态下)
			if s.BarCntr == 0 {
				if common.LOADING == s.GetState() {
					OpenAr, _ := s.GetParamFloat(s.GetName(), "OpenAr")
					if OpenAr > 0.1 {
						s.RiskAct = OpenAr
						s.Log(common.ALARMINFO, fmt.Sprintf("读出上次OpenAr， RiskAct: %f = Ar: %f) ", s.RiskAct, OpenAr))
					} else {
						Ar, err := s.GetParamFloat("RiskFunction", "Ar")
						if nil != err {
							s.Log(common.ALARMERROR, err.Error())
							s.RiskAct = 0
						} else {
							s.RiskAct = Ar
							s.Log(common.ALARMINFO, fmt.Sprintf("获得风控默认Ar，RiskAct: %f = Ar: %f) ", s.RiskAct, Ar))
						}
					}
				}
			}

			// (6) 交易 ---------------------------------------------------------------------------
			miss := s.minmove * 1.0
			tradePrice := s.K.Open[klinekey][0]
			lastQuote := common.LastPrice{}
			if common.RUNNING == s.GetState() {
				var err error
				lastQuote, err = s.GetLastPrice(s.Contract.MainTrade)
				if nil != err {
					log.Errorf("Cannot get latest price: in strategy error: %v", err)
					s.Stop(0)
				}
				tradePrice = lastQuote.LastPrice
			}

			if s.GetMainVirtualPosition() <= 0 && 0 < s.SimPositionRate {
				// 开多
				// 跳变情况
				if s.K.Open[klinekey][0] > s.max_Hs {
					s.InPrice = s.K.Open[klinekey][0] + miss
				} else {
					// 正常情况
					s.InPrice = s.max_Hs + s.minmove + miss
				}
				s.InPosRV = s.ClrWt * s.RV[0]
				s.ClrThr = s.InPrice - s.InPosRV

				s.InPos = math.Floor(s.RiskAct / (s.InPosRV * s.mutiplier))
				if s.InPos > 0 {
					if common.REPLAY == s.GetState() || common.LOADING == s.GetState() {
						if s.GetMainVirtualPosition() < 0 {
							_, _ = s.Buy(s.Contract.MainTrade, s.InPrice, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						}
						_, _ = s.Buy(s.Contract.MainTrade, s.InPrice, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
					} else if common.RUNNING == s.GetState() {
						s.TradePrc = tradePrice + miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						if s.GetMainVirtualPosition() < 0 {
							orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
							if err == nil {
								_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
							}
						}
						orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
						s.SetParamFloat("OpenRV", s.InPosRV)
						s.SetParamFloat("OpenAr", s.RiskAct)
					} else {
						s.Stop(0)
					}
				}
			} else if s.GetMainVirtualPosition() >= 0 && 0 > s.SimPositionRate {
				// 开空
				// 跳变情况
				if s.K.Open[klinekey][0] < s.min_Ls {
					s.InPrice = s.K.Open[klinekey][0] - miss
				} else {
					// 正常情况
					s.InPrice = s.min_Ls - s.minmove - miss
				}
				s.InPosRV = s.ClrWt * s.RV[0]
				s.ClrThr = s.InPrice + s.InPosRV

				s.InPos = math.Floor(s.RiskAct / (s.InPosRV * s.mutiplier))
				if s.InPos > 0 {
					if common.REPLAY == s.GetState() || common.LOADING == s.GetState() {
						if s.GetMainVirtualPosition() > 0 {
							_, _ = s.Sell(s.Contract.MainTrade, s.InPrice, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						}
						_, _ = s.Sell(s.Contract.MainTrade, s.InPrice, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
					} else if common.RUNNING == s.GetState() {
						s.TradePrc = tradePrice - miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						if s.GetMainVirtualPosition() > 0 {
							orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
							if err == nil {
								_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
							}
						}
						orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
						s.SetParamFloat("OpenRV", s.InPosRV)
						s.SetParamFloat("OpenAr", s.RiskAct)
					} else {
						s.Stop(0)
					}
				}
			} else if s.GetMainVirtualPosition() > 0 {
				// 持多
				if clr_flg {
					if common.REPLAY == s.GetState() || common.LOADING == s.GetState() {
						s.TradePrc = s.ClrPrice - miss
						_, _ = s.Sell(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
					} else if common.RUNNING == s.GetState() {
						s.TradePrc = tradePrice - miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
					} else {
						s.Stop(0)
					}
					s.SimPositionRate = 0 // sim_pos_rate 控制着实际开平仓, 所以全平后必须清零
				}
			} else if s.GetMainVirtualPosition() < 0 {
				// 持空
				if clr_flg {
					if common.REPLAY == s.GetState() || common.LOADING == s.GetState() {
						s.TradePrc = s.ClrPrice + miss
						_, _ = s.Buy(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
					} else if common.RUNNING == s.GetState() {
						s.TradePrc = tradePrice + miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
					} else {
						s.Stop(0)
					}
					s.SimPositionRate = 0 // sim_pos_rate 控制着实际开平仓, 所以全平后必须清零
				}
			}
			// 记录数据
			s.AddIndicator(
				false,
				[]string{"ClrLin", "ShortMA", "LongMA", "max_Hs", "min_Ls", "max_coverHs", "min_coverLs"},
				[]string{"ATR"},
				strategy.ColumnValue{Name: "RV", Value: strconv.FormatFloat(s.RV[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ATR", Value: strconv.FormatFloat(s.ATR[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ShortMA", Value: strconv.FormatFloat(s.ShortLine[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "LongMA", Value: strconv.FormatFloat(s.LongLine[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ClrLin", Value: strconv.FormatFloat(s.ClrThr, 'f', 4, 64)},
				strategy.ColumnValue{Name: "max_Hs", Value: strconv.FormatFloat(s.max_Hs, 'f', 4, 64)},
				strategy.ColumnValue{Name: "min_Ls", Value: strconv.FormatFloat(s.min_Ls, 'f', 4, 64)},
				strategy.ColumnValue{Name: "max_coverHs", Value: strconv.FormatFloat(s.max_coverHs, 'f', 4, 64)},
				strategy.ColumnValue{Name: "min_coverLs", Value: strconv.FormatFloat(s.min_coverLs, 'f', 4, 64)},
			)
			s.BarCntr += 1
		} else {
			// Offset --------------------- 当前K线刷新刷新
			if common.RUNNING == s.GetState() {
				// (1) 开仓条件 :  act_flg -----------------------------------------------------------
				Close0 := s.K.Close[klinekey][0]
				act_flg := 0
				if s.ShortLine[0] > s.LongLine[0] && Close0 > s.max_Hs {
					act_flg = 1
				} else if s.ShortLine[0] < s.LongLine[0] && Close0 < s.min_Ls {
					act_flg = -1
				}

				// (2) 平仓条件 :  clr_flg --------------------------------------------------------------
				clr_flg := false
				// (3) 平仓/减仓 --------------------------------------------------------------
				if s.GetMainVirtualPosition() > 0 {
					if Close0 < s.ClrThr || Close0 < s.min_coverLs {
						s.ClrPrice = Close0
						clr_flg = true
					}
				} else if s.GetMainVirtualPosition() < 0 {
					if Close0 > s.ClrThr || Close0 > s.max_coverHs {
						s.ClrPrice = Close0
						clr_flg = true
					}
				}
				if act_flg != 0 {
					// 读取风控模块 RiskControl 的 风险参数 (Running 状态下)
					Ar, err := s.GetParamFloat("RiskFunction", "Ar")
					if nil != err {
						s.Log(common.ALARMERROR, err.Error())
						s.RiskAct = 0
					} else {
						s.RiskAct = Ar
						s.Log(common.ALARMINFO, fmt.Sprintf("RiskAct: %f = Ar: %f) ", s.RiskAct, Ar))
					}
				}

				// (4) 开仓 ----------------------------------------------------------------------------------
				if act_flg == 1 {
					if s.GetMainVirtualPosition() == 0 {
						s.SimPositionRate = 1
						//s.RiskAct = s.RiskBudget
					}
				} else if act_flg == -1 {
					if s.GetMainVirtualPosition() == 0 {
						s.SimPositionRate = -1.0
						//s.RiskAct = s.RiskBudget
					}
				}
				// (6) 交易 ---------------------------------------------------------------------------
				miss := s.minmove * 1.0
				lastQuote, err := s.GetLastPrice(s.Contract.MainTrade)
				if nil != err {
					log.Errorf("Cannot get latest price: in strategy error: %v", err)
					s.Stop(0)
				}
				tradePrice := lastQuote.LastPrice
				if s.GetMainVirtualPosition() <= 0 && 0 < s.SimPositionRate {
					// 开多
					s.InPrice = Close0 //记录开仓触发价\计算风险金对应仓位, 用 MainKline 的最新价!
					s.InPosRV = s.ClrWt * s.RV[0]
					s.ClrThr = s.InPrice - s.InPosRV
					s.InPos = math.Floor(s.RiskAct / (s.InPosRV * s.mutiplier))
					if s.InPos > 0 {
						s.TradePrc = tradePrice + miss //实际交易价格用 MainTrade 的最新价
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						if s.GetMainVirtualPosition() < 0 {
							orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
							if err == nil {
								_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
							}
						}
						orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
						s.SetParamFloat("OpenRV", s.InPosRV)
						s.SetParamFloat("OpenAr", s.RiskAct)
					}
				} else if s.GetMainVirtualPosition() >= 0 && 0 > s.SimPositionRate {
					// 开空
					s.InPrice = Close0 //记录开仓触发价\计算风险金对应仓位, 用 MainKline 的最新价!
					s.InPosRV = s.ClrWt * s.RV[0]
					s.ClrThr = s.InPrice + s.InPosRV
					s.InPos = math.Floor(s.RiskAct / (s.InPosRV * s.mutiplier))
					if s.InPos > 0 {
						s.TradePrc = tradePrice - miss //实际交易价格用 MainTrade 的最新价
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						if s.GetMainVirtualPosition() > 0 {
							orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
							if err == nil {
								_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
							}
						}
						orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, int64(s.InPos), common.PRICELIMIT, common.OFFSETOPEN, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
						s.SetParamFloat("OpenRV", s.InPosRV)
						s.SetParamFloat("OpenAr", s.RiskAct)
					}
				} else if s.GetMainVirtualPosition() > 0 {
					// 持多
					if clr_flg {
						s.TradePrc = tradePrice - miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						orderSN, err := s.Sell(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
					}
					s.SimPositionRate = 0 // sim_pos_rate 控制着实际开平仓, 所以全平后必须清零
				} else if s.GetMainVirtualPosition() < 0 {
					// 持空
					if clr_flg {
						s.TradePrc = tradePrice + miss
						s.TradePrc = utils.ValidLimitPrice(s.TradePrc, lastQuote)
						orderSN, err := s.Buy(s.Contract.MainTrade, s.TradePrc, 0, common.PRICELIMIT, common.OFFSETCLOSE, common.NONE)
						if err == nil {
							_ = s.TailOrder(orderSN, 500, 3, 5.0, "")
						}
					}
					s.SimPositionRate = 0 // sim_pos_rate 控制着实际开平仓, 所以全平后必须清零
				}
			}
			s.AddIndicator(
				true,
				[]string{"ClrLin", "ShortMA", "LongMA", "max_Hs", "min_Ls", "max_coverHs", "min_coverLs"},
				[]string{"ATR"},
				strategy.ColumnValue{Name: "RV", Value: strconv.FormatFloat(s.RV[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ATR", Value: strconv.FormatFloat(s.ATR[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ShortMA", Value: strconv.FormatFloat(s.ShortLine[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "LongMA", Value: strconv.FormatFloat(s.LongLine[0], 'f', 4, 64)},
				strategy.ColumnValue{Name: "ClrLin", Value: strconv.FormatFloat(s.ClrThr, 'f', 4, 64)},
				strategy.ColumnValue{Name: "max_Hs", Value: strconv.FormatFloat(s.max_Hs, 'f', 4, 64)},
				strategy.ColumnValue{Name: "min_Ls", Value: strconv.FormatFloat(s.min_Ls, 'f', 4, 64)},
				strategy.ColumnValue{Name: "max_coverHs", Value: strconv.FormatFloat(s.max_coverHs, 'f', 4, 64)},
				strategy.ColumnValue{Name: "min_coverLs", Value: strconv.FormatFloat(s.min_coverLs, 'f', 4, 64)},
			)
		}
	} else if klinekey.Symbol == s.Contract.SubKline.Symbol {
		// Sub ---------------------- 次合约
	}
}

func main() {
	// 实例化策略
	turtleTrading := &TurtleTrading{Base: strategy.Base{}}

	// 注册行情回调函数
	turtleTrading.RegOnInit(turtleTrading.OnInit)
	turtleTrading.RegOnTick(turtleTrading.OnTick)
	turtleTrading.RegOnKline(turtleTrading.OnKline)

	// pluginMap is the map of plugins we can dispense.
	fullName, _ := os.Executable()
	fullName = strings.ReplaceAll(fullName, "\\", "/")
	pluginName := path.Base(path.Dir(fullName))

	var pluginMap = map[string]plugin.Plugin{
		pluginName: &strategy.StrategyPlugin{Impl: turtleTrading},
	}
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: strategy.HandshakeConfig,
		Plugins:         pluginMap,
	})
}
