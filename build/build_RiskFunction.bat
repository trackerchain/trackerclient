@echo off
cd %~dp0

echo RiskFunction compile release begin
cd ./RiskFunction
..\..\bin\tracker sta stop --name=RiskFunction
go build -gcflags="all=-N -l"
if not exist ..\..\plugin\RiskFunction (
  md ..\..\plugin\RiskFunction
)
copy .\RiskFunction.exe ..\..\plugin\RiskFunction /Y
if not exist ..\..\plugin\RiskFunction\RiskFunction.toml (
  copy .\RiskFunction.toml ..\..\plugin\RiskFunction /Y
)
..\..\bin\tracker sta active --name=RiskFunction
cd %~dp0
echo RiskFunction compile release end
