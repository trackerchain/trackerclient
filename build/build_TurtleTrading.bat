@echo off
cd %~dp0

echo TurtleTrading compile release begin
cd ./TurtleTrading
..\..\bin\tracker sta stop --name=TurtleTrading
go build -gcflags="all=-N -l"
if not exist ..\..\plugin\TurtleTrading (
  md ..\..\plugin\TurtleTrading
)
copy .\TurtleTrading.exe ..\..\plugin\TurtleTrading /Y
if not exist ..\..\plugin\TurtleTrading\TurtleTrading.toml (
  copy .\TurtleTrading.toml ..\..\plugin\TurtleTrading /Y
)
..\..\bin\tracker sta active --name=TurtleTrading
cd %~dp0
echo TurtleTrading compile release end
